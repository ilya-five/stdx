<h3 align='center'>Description</h3>
Several useful (I hope) templates dependent on std only.

<h3 align='center'>License</h3>
Public Domain except for `scope_guard/finally.h` which (being copied from stackexchange) is under CC-BY-SA license.