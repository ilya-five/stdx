#ifndef STDX_TUPLE_H
#define STDX_TUPLE_H

#include <../tuple/iterate_over_tuple.h>
#include <../tuple/misc.h>
#include <../tuple/unwrap_and_forward.h>

#endif // !STDX_TUPLE_H
