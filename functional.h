#ifndef STDX_FUNCTIONAL_H
#define STDX_FUNCTIONAL_H

#include <../functional/function_traits.h>
#include <../functional/has_result_type.h>

#endif // !STDX_FUNCTIONAL_H
