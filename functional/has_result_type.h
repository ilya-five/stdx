#ifndef STDX_HAS_RESULT_TYPE_H
#define STDX_HAS_RESULT_TYPE_H

namespace stdx 
{
template <typename T>
class has_result_type {
    // Types "yes" and "no" are guaranteed to have different sizes,
    // specifically sizeof(yes) == 1 and sizeof(no) == 2.
    typedef char yes[1];
    typedef char no[2];
 
    template <typename C>
    static yes& test(typename C::result_type*);
 
    template <typename>
    static no& test(...);
 
public:
    // If the "sizeof" the result of calling test<T>(0) would be equal to the sizeof(yes),
    // the first overload worked and T has a nested type named result_type.
    static constexpr bool value = sizeof(test<T>(0)) == sizeof(yes);
};

} // namespace stdx

#endif // !STDX_HAS_RESULT_TYPE_H
