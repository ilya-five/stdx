#ifndef STDX_FUNCTIONAL_FUNCTION_TRAITS_H
#define STDX_FUNCTIONAL_FUNCTION_TRAITS_H

#include <cstddef>

#include <functional>
#include <tuple>

namespace stdx 
{
template<typename T> 
struct function_traits;  

template<typename R, typename ...Args> 
struct function_traits<std::function<R(Args...)>>
{
    static const size_t nargs = sizeof...(Args);

    typedef R result_type;

    template <size_t i>
    struct arg
    {
        typedef typename std::tuple_element<i, std::tuple<Args...>>::type type;
    };
};

} // namespace stdx

#endif // !STDX_FUNCTIONAL_FUNCTION_TRAITS_H
