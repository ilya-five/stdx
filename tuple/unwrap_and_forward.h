#ifndef STDX_TUPLE_UNWRAP_AND_FORWARD_H
#define STDX_TUPLE_UNWRAP_AND_FORWARD_H

#include <tuple>

namespace stdx {
namespace detail {

template<int...> struct int_tuple {};

// make_indexes_impl is a helper for make_indexes
template<int I, typename IntTuple, typename... Types>
struct make_indexes_impl;

template<int I, int... Indexes, typename T, typename... Types>
struct make_indexes_impl<I, int_tuple<Indexes...>, T, Types...>
{
    typedef typename make_indexes_impl<I+1, int_tuple<Indexes..., I>, Types...>::type type;
};

template<int I, int... Indexes>
struct make_indexes_impl<I, int_tuple<Indexes...> > {
    typedef int_tuple<Indexes...> type;
};

} // namespace detail

template<typename... Types>
struct make_indexes : detail::make_indexes_impl<0, detail::int_tuple<>, Types...> { };

template<typename F, typename Convert, int... Indexes, typename... Args>
inline auto
unwrap_and_forward(const F& f, const Convert& convert, detail::int_tuple<Indexes...>, 
        std::tuple<Args...>& args) 
-> decltype(f(convert(std::get<Indexes>(args))...))
{
    return f(convert(std::get<Indexes>(args))...);
}

template<typename F, typename Convert, int... Indexes, typename... Args>
inline auto
unwrap_and_forward(F& f, const Convert& convert, detail::int_tuple<Indexes...>, 
        std::tuple<Args...>& args) 
-> decltype(f(convert(std::get<Indexes>(args))...))
{
    return f(convert(std::get<Indexes>(args))...);
}

} // namespace stdx

#endif // !STDX_TUPLE_UNWRAP_AND_FORWARD_H
