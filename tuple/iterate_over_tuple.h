#ifndef STDX_ITERATE_OVER_TUPLE_H
#define STDX_ITERATE_OVER_TUPLE_H

#include <tuple>

namespace stdx {
namespace detail {

template <int I, int TSize, typename Tuple>
struct iterate_over_tuple_impl
    : public iterate_over_tuple_impl<I + 1, TSize, Tuple>
{
    template <typename Function>
        void operator () (const Function& f, Tuple& t)
        {
            f(std::get<I>(t));
            iterate_over_tuple_impl<I + 1, TSize, Tuple>::operator () (f, t);
        }
};

template <int I, typename Tuple>
struct iterate_over_tuple_impl<I, I, Tuple> {
    template <typename Function>
        void operator () (const Function& f, Tuple& t) {}
};

} // namespace detail

template <typename Function, typename... Args>
void iterate_over_tuple(const Function& f, std::tuple<Args...>& t)
{
    detail::iterate_over_tuple_impl<0, sizeof...(Args), std::tuple<Args...>>() (f, t);
}

} // namespace stdx

#endif // !STDX_ITERATE_OVER_TUPLE_H
