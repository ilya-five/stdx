#ifndef K3DSDK_STDX_TUPLE_MISC_H
#define K3DSDK_STDX_TUPLE_MISC_H

#include <tuple>

namespace stdx {

/// Used to pass U instance in variadic expression submitting unpacked parameters into T parameter.
/// Necessary for initializing tuple of types with shared owner and without copy constructor.
template<typename T, typename U> U& variadic_id(U& x){return x;}

} // namespace stdx

#endif // !K3DSDK_STDX_TUPLE_MISC_H
