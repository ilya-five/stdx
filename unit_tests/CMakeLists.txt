FIND_PACKAGE( Boost 1.55 COMPONENTS unit_test_framework REQUIRED )

INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR})

ADD_EXECUTABLE(unit_tests main.cpp
    functional_tests.cpp
    scope_guard_tests.cpp
    tuple_tests.cpp
    )

TARGET_LINK_LIBRARIES( unit_tests ${Boost_LIBRARIES} )
