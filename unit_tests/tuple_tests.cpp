#include <../tuple.h>

#include <exception>
#include <functional>
#include <tuple>

#include <boost/test/unit_test.hpp>

using namespace std;
using namespace stdx;
using namespace boost;

BOOST_AUTO_TEST_SUITE( tuple_suite )
        
struct accum { 
    int operator()(const int& a, const int& b) {
        acc += a + b;
        return acc;
    }
    int acc{0};
};

BOOST_AUTO_TEST_CASE( unwrap_and_forward_test )
{
    auto t = make_tuple(1, 2);
    BOOST_CHECK_EQUAL( -3, unwrap_and_forward(plus<int>(), negate<int>(), 
            typename make_indexes<int, int>::type(), t) );
    auto acc = accum();
    BOOST_CHECK_EQUAL( -3, unwrap_and_forward(acc, negate<int>(), 
            typename make_indexes<int, int>::type(), t) );
    BOOST_CHECK_EQUAL( -3, acc.acc );
}
        
BOOST_AUTO_TEST_SUITE_END()

