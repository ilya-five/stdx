#include <../functional.h>

#include <exception>

#include <boost/test/unit_test.hpp>

using namespace std;
using namespace stdx;
using namespace boost;

BOOST_AUTO_TEST_SUITE( functional_suite )
        
BOOST_AUTO_TEST_CASE( has_result_type_test )
{
    BOOST_CHECK( has_result_type<plus<int>>::value );
    BOOST_CHECK( !has_result_type<int>::value );
}
        
BOOST_AUTO_TEST_SUITE_END()
