#include <../scope_guard/finally.h>

#include <exception>

#include <boost/test/unit_test.hpp>

using namespace std;
using namespace stdx;
using namespace boost;

BOOST_AUTO_TEST_SUITE( scope_guard_suite )
        
BOOST_AUTO_TEST_CASE( finally_test )
{
    bool check = false;
    {
        auto guard = finally([&check]{ check = true; });
    }
    BOOST_CHECK( check );
}
        
BOOST_AUTO_TEST_SUITE_END()
