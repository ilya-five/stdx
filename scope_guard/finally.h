#ifndef STDX_SCOPE_GUARD_FINALLY_H
#define STDX_SCOPE_GUARD_FINALLY_H

// According to stackexchange.com licensing this file is under CC-BY-SA license.

#include <memory>
#include <utility>

namespace stdx
{

// Copied from here (Deduplicator answer) - https://softwareengineering.stackexchange.com/questions/308732/preferable-design-of-scope-guard-in-c
template<class F>
auto finally(F f) noexcept(noexcept(F(std::move(f)))) {
    auto x = [f = std::move(f)](void*){ f(); };
    return std::unique_ptr<void, decltype(x)>((void*)1, std::move(x));
}

} // namespace stdx

#endif // !STDX_SCOPE_GUARD_FINALLY_H
